import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Main {
    public static void main(String[] args) {
        String huruf = "";
        Integer karakter = 0;
        Integer vokal = 0;
        Integer konsonan = 0;
        Scanner keyboard = new Scanner(System.in);

        System.out.print("Masukkan huruf: ");
        huruf = keyboard.nextLine();

        Pattern p = Pattern.compile("[aiueo]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(huruf);

        for (int i = 0; i<huruf.length(); i++) {
            char c = huruf.charAt(i);
            if (m.find())
            {
                karakter += 1;
                vokal += 1;
            } else if (Character.toString(c).equals(" ")) {
                continue;
            } else {
                karakter += 1;
                konsonan += 1;
            }
        }

        System.out.printf("Jumlah Vokal: %d%n", vokal);
        System.out.printf("Jumlah Konsonan: %d%n", konsonan);
        System.out.printf("Jumlah Karakter: %d%n", karakter);
    }
}